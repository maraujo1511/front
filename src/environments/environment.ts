// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    api_token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwiaWF0IjoxNjEzOTQwNDMxLCJleHAiOjE2MTQwMjY4MzF9.WsYMjJ02TrYCwG2aD4Pm10cZPkLR6VO5yszmdRhCqms",
    api_url: "https://firm-buffer-305323.uc.r.appspot.com/api",
    api_translate: "https://libretranslate.com/translate",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
